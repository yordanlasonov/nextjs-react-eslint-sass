const withSASS = require('@zeit/next-sass');

module.exports = withSASS({
  cssModules: true,
  distDir: 'dist',
  
  webpack: (config, { dev }) => {
    return config
  }
});