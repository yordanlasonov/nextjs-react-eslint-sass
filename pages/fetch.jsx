import Link from 'next/link';
import Head from '../components/head';
import Nav from '../components/nav';
import Global from './_global';
import { Fetch } from 'react-data-fetching';
import styles from './fetch.scss';
import classNames from 'classnames';


export default class extends React.Component {
  render() {
    return (
      <Global>
        <Head title="Home" />
        <Nav />
        <Fetch
          loader={<img src='../static/loading.svg'/>}// Replace this with your lovely handcrafted loader
          url="https://api.github.com/users/octocat"
          timeout={5000}>
          {({ data }) => (
            <div>
              <h1 className={classNames({[styles.headline]: true, [styles.headlineUnderline]: true})}>Username</h1>
              <p>{data.name}</p>
              <p>{data.url}</p>
              <p>{data.type}</p>
            </div>
          )}

        </Fetch>
  
      </Global>
    );
  }
}
