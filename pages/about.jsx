import Link from 'next/link';
import Head from '../components/head';
import Nav from '../components/nav';
import Global from './_global';

export default () => (
  <Global>
    <Head title="Home" />
    <Nav />

    <div className="hero">
      <h1 className="title">About!</h1>
      <p className="description">To get started, edit <code>pages/index.js</code> and save to reload.</p>

      <div className="row">
        <Link href="https://github.com/zeit/next.js#getting-started">
          <a className="card">
            <h3>Lorem Ipsum &rarr;</h3>
            <p>Learn more about Next on Github and in their examples</p>
          </a>
        </Link>
      </div>
    </div>
  </Global>
);
