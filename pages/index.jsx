import React from 'react';
import Link from 'next/link';
import Head from '../components/head';
import Nav from '../components/nav';
import Global from './_global';
import styles from'./index.scss';

export default class extends React.Component {
  render() {
    return (
      <Global>
        <Head title="Home" />
        <Nav/>
        <h1>Welcome to React-Next-ESlint-Sass Boilerplate!</h1>
      </Global>
    );
  }
}
